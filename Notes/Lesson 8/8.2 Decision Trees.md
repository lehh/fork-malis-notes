## An example

![[./Images/Pasted image 20240103154905.png]]

### A first decision tree

![[./Images/Pasted image 20240103154945.png]]


With this decision tree Alfred gets misclassified.

Exercise: What is the smallest tree you can construct that properly classifies all the data? How many nodes?

![[./Images/Pasted image 20240103155107.png]]


Learning the simplest (smallest) decision tree is an NP-complete problem.
Solution - Resort to a greedy approach:
- Start from empty decision tree
- Split on next best feature
- Recurse

Goal: Build a maximally compact tree with only pure leaves

## Impurity functions

Greedy strategy: We keep splitting the data to minimize an impurity function that measures label purity among the children.

Formalization & Definitions:
- Data $\mathcal{D} = \{(x_1,y_1),\cdots,(x_n,y_N)\}$, $y_1 \in \{1,\cdots,K\}$
- $K$: total number of classes
- $\mathcal{D}_K \subset D$ where $\mathcal{D}_k = \{(x,y) \in \mathcal{D} : y = k\}$ 
- $\mathcal{D} = \mathcal{D}_1 \cup \mathcal{D}_2 \cdots \cup \mathcal{D}_K$

Given the previous definitions, the fraction of inputs in $D$ with label $k$ is:
$$p_k = \frac{\lvert \mathcal{D}_k \rvert}{\lvert \mathcal{D} \rvert}$$

Gini impurity:
$$G(\mathcal{D}) = \sum_{k=1}^K p_k(1-p_k)$$

If $K=2$ then:
$G(\mathcal{D}) = p_1(1-p_1)+p_2(1-p_2) = p_1(1-p_1)+(1-p_1)p_1 = 2p(1-p)$

Interpretation:
- Maximum impurity at $p=0.5$ since there is 50-50 for both classes (root node case)
- Our goal is to make the Gini impurity zero
- Low Gini: Dominated by one class.
- High Gini: There is no dominating class

![[./Images/Pasted image 20240103161014.png]]

In other words: Max impurity, if we have 0.5 we have same number elements for both classes. If impurity 0 then we have homogeneus data, all of one class or the other

### Entropy

Let us recall $p_k$ the fraction of inputs for a given label, what is the worst case scenario for the leave of a tree?
Answer: $q_1=q_2=\cdots=q_k = \frac{1}{K}$. We don't want to have a uniform distribution.

Idea: Let us measure impurity as how close we are to the uniform distribution. We will use the Kullback-Liebler divergence to measure the closeness.

Kullback-Liebler (KL-)divergence):
$$KL(p \lvert\rvert q) = \sum_{k=1}^K p_k \log\frac{p_k}{q} \ge 0$$


$$KL(p \lvert\rvert q) = \sum_{k=1}^K p_k \log p_k - p_k \log q$$

Our goal is to measure how far $p_k$ are from the uniform distribution. Let us then replace accordingly for such a case.

$$
\begin{aligned}
& KL\left(p \lvert\rvert \frac{1}{K}\right)  = \sum_{k=1}^K p_k \log p_k - p_k \log \frac{1}{K} \\ & =  \sum_{k=1}^K p_k \log p_k+ p_k \log K \\ & =  \sum_{k=1}^K p_k \log p_k + \log K \sum_{k=1}^K p_k
\end{aligned}
$$But the second term is a constant so we can remove it.
Moreover we know that $\sum_{k=1}^K p_k = 1$. Which leads to:

$$KL\left(p \lvert\rvert \frac{1}{K}\right) = \sum_{k=1}^K p_k \log p_k$$
We want to make sure that $p_k$ is as far as possible from the uniform distribution. This means:

$$\mathop{\max}\limits_p KL\left(p \lvert\rvert \frac{1}{K}\right) = \mathop{\max}\limits_p \sum_{k=1}^K p_k \log p_k$$
Because we prefer minimization we can use:
$$\mathop{\min}\limits_p KL\left(p \lvert\rvert \frac{1}{K}\right) = \mathop{\min}\limits_p - \sum_{k=1}^K p_k \log p_k$$

This quantity represents the entropy $H(S)$:
$$\mathop{\min}\limits_p KL\left(p \lvert\rvert \frac{1}{K}\right) = \mathop{\min}\limits_p H(\mathcal{D})$$


## Estimating the entropy of a tree

The entropy of a tree can be estimated as:
$$H(\mathcal{D}) = p^LH(\mathcal{D_L}) + p^RH(\mathcal{D}_R)$$

Where: 
$$p^L = \frac{\lvert \mathcal{D}_L \rvert}{\lvert \mathcal{D} \rvert}$$

$$p^R = \frac{\lvert \mathcal{D}_R \rvert}{\lvert \mathcal{D} \rvert}$$

![[./Images/Pasted image 20240103162943.png]]



### The algorithm
1. Compute the impurity function for every attribute of dataset $S = \mathcal{D}$
2. Split the set S into subsets using the attribute for which the resulting impurity function is minimal after splitting (greedy approach)
3. Make a decision tree node containing that attribute
4. Recurse on subsets with remaining attributes 

A stopping criterion is required to determine when to stop

### Iterative Dichotomiser 3 (ID3) Algorithm

The ID3 algorithm follows the scheme previously presented and uses the following set of rules to decide when to stop:
- Rule 1 Every element in the subset belongs to the same class. Node becomes a leaf node
- Rule 2 There are no more attributes to be selected, but the examples still do not belong to the same class. Node becomes leaf node labeled with the majority class. In other words: we have run out of features to split on, but it will happen that the leaf is not pure, so you just pick the majority in the leaf
- Rule 3 There are no examples in the subset, as no example in the parent set matches a specific value of the selected attribute. Create leaf node labeled with the majority class of the parent.

#### Iterative Dichotomiser 3 (ID3) Algorithm: Splits

The greedy approach to splitting a parent node into sub-nodes requires to try all
features/attributes and all possible splits. The chosen attribute a is the one that will minimize the impurity for a given threshold $t$ defining the split. More formally,

$$S^L = \{(x,y) \in S : x^{(a)} \le t\}$$

$$S^R = \{(x,y) \in S : x^{(a)} \le t\}$$

#### An example
![[./Images/Pasted image 20240103164204.png]]


![[./Images/Pasted image 20240103164210.png]]



## Limitations: Myopia

Let us have a look at the XOR and try to train a decision tree that learns to classify its samples properly.

![[./Images/Pasted image 20240103164349.png]]

- Decision trees suffer from myopia
- They cannot see further beyond the current node
- Therefore, in practice, it is recommended to "keep trying" and let the tree grow
- Once the tree has grown, tree pruning is done

Here is the solution of the XOR problem:

![[Pasted image 20240103164428.png]]

## The overfitting probelm

![[Pasted image 20240103164523.png]]

![[Pasted image 20240103164448.png]]

- Must use tricks to find simple trees
- Fixed depth/Early stopping
 - Pruning
- Use ensembles of different trees: Random forests


### CART: Classification and Regression Trees

Decision trees can also be used for regression, i.e. $y \in \mathbb{R}$. In such a case, the impurity needs to change.

Impurity: Squared Loss.

$$L(R) = \frac{1}{R} \sum_{(x,y)\in R} (y-\bar{y}_r)^2$$
Where R denotes a region
$$\bar{y}_R = \frac{1}{R} \sum_{(x,y)\in R} y$$
is the average label of the region R.

Important: Finding the best binary partition in terms of minimum sum of squares is generally computationally infeasible. A greedy approach is preferred, using the same principle as for classification.


### Example

![[./Images/img10.png]]


![[./Images/img11.png]]

![[./Images/img5.png]]