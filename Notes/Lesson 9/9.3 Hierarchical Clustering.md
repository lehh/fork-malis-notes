$K$-means clustering requires to pre-specify the number of clusters $K$.
Results from $K$-means are somehow random: they depend on the random initialization from which the algorithm started. And each algorithm run will produce different results. Also choosing K correctly is an open question.


Hierarchical clustering is an alternative approach that:
1. Does not require a pre-defined $K$
2. Provides a deterministic answer

Two types:
1. Bottom-up or agglomerative
2. Top-down or divisive

Example:
![[./Images/Pasted image 20240128115144.png]]
Next step:
![[./Images/Pasted image 20240128115150.png]]
Next step:
![[./Images/Pasted image 20240128115201.png]]
Next step:
![[./Images/Pasted image 20240128115137.png]]

Next step is a single circle containing all the points, why? why not create (b,d)? because we have already decided (a,c,b) are together, so in the next step we can't break that cluster

## Agglomerative Hierarchical Clustering

Algorithm
1. Start with each point in its own
2. Identify the two closest clusters. Merge
3. Repeat until all points are in a single cluster

Results can be visualized using a dendrogram The Y-axis reflects the distance between the clusters that got merged at that step.

![[./Images/Pasted image 20240128115311.png]]

## Linkage

Let $d_{ij} = d(x_i,x_j)$ denote the dissimilarity between samples $x_i,x_j$.
- At the first step of hierarchical clustering, each cluster is a single point
- The merging is done over two observations showing the lowest dissimilarity
- After that, we need to think about dissimilarities (distances) between sets (clusters) not single points

Linkage: Dissimilarity between two clusters: $d(G,H)$ denotes the dissimilarity between sets $G$ and $H$.

### Linkage types

- Complete: Maximal inter-cluster dissimilarity. Computes all pairwise dissimilarities between observations of cluster G and cluster H. Records the largest
- Single: Minimal inter-cluster dissimilarity. Computes all pairwise dissimilarities between observations of cluster G and cluster H. Records the smallest.
- Average: Mean inter-cluster dissimilarity. Computes all pairwise dissimilarities between observations of cluster G and cluster H. Records the average.
- Centroid Dissimilarity: between the centroid for cluster G and centroid for cluster H. Take two clusters and join the clusters with the closest centroids
- Ward: Minimizes the variance of the clusters to be merged. At each step find the pair of clusters that leads to minimum increase in total within-cluster variance after merging. Add to the cluster that increases the least the variance inside that cluster

![[./Images/Pasted image 20240128115626.png]]

### Single linkage
The dissimilarity between $G,H$ is the smallest dissimilarity between two points in different groups. It minimizes the distance between the closest observations of pairs of clusters. It is a nearest-neighbor linkage:
$$d_{\mathrm{single}}(G,H) = \min_{i\in G, j\in H} d(x_i,x_j)$$
![[./Images/Pasted image 20240128115921.png]]

Example:
$N=60$, $d_{ij} = \lvert\lvert x_i - x_j \rvert\rvert$, cut at $h=0.9$.

![[./Images/Pasted image 20240128115936.png]]

The interpretation is: For each point, there is another point in its cluster such that $d\le0.9$. Points further away than 0.9 are in different cluster.

### Complete linkage

The dissimilarity between $G,H$ is the largest dissimilarity between two points in different groups. It minimizes the maximum distance between observations of pairs of clusters. It is a farthest-neighbor linkage:
$$d_{\mathrm{complete}}(G,H) = \max_{i\in G, j\in H} d(x_i,x_j)$$

![[./Images/Pasted image 20240128120151.png]]

Example:
$N=60$, $d_{ij} = \lvert\lvert x_i - x_j \rvert\rvert$, cut at $h=5$.
![[./Images/Pasted image 20240128120220.png]]

For each point, every other point in the same cluster satisfies $d_{ij} \le 5$.

### Average linkage
The dissimilarity between $G,H$ is is the average dissimilarity over all points in opposite groups. It minimizes the average of the distances between all observations of pairs of clusters.
$$d_{\mathrm{AVG}}(G,H) = \frac{1}{\lvert G \rvert \lvert H \rvert} \sum_{i\in G, j\in H} d(x_i,x_j)$$

![[./Images/Pasted image 20240128120329.png]]

Example:
$N=60$, $d_{ij} = \lvert\lvert x_i - x_j \rvert\rvert$, cut at $h=2.5$.
![[./Images/Pasted image 20240128120635.png]]

No good interpretation.


## Chaining and Crowding

Chaining:
- To merge two groups, single linkage only needs for one pair of points to be close. To make them part of the same cluster, this creates very spread out clusters
- Clusters can be too spread out and not compact enough
Crowding:
- Complete linkage avoids chaining but suffers from crowding
- Because its score is based on the worst-case dissimilarity, a point can be closer to points in other clusters than to points in its own cluster.
- Compact clusters that are not well separated Average linkage tries to balance this

Average linkage tries to balance this, but we can't interpret it.

Single is very spread out!
![[./Images/Pasted image 20240128120845.png]]


### Disadvantages of average linkage

The dendrogram does not give a nice interpretation of the cut. Results can change. Example: Apply a monotone increasing transformation to the dissimilarity measure, for example $d \rightarrow d^2$. This is problematic when not sure of which measure to use. Single and complete do not have this problem.

![[./Images/Pasted image 20240128121009.png]]

But we get completely different results.


## Dissimilarity measure

- The choice of linkage has strong effects on structure and quality of resulting clusters
- The choice of the dissimilarity/similarity measure is as or more important than the linkage
- Using a dissimilarity measure is easy.
- Finding the good measure is the real challenge
- Warning: In the literature, you may find both references to similarity or dissimilarity measures. Large dissimilarity implies, small similarity and vice versa.

Philosophical question: What does it mean for two observations to be similar?


## Clustering comparison

K-means
- Low memory usage
- Good implementation: O(n)
- Sensitive to initialization
- Number of clusters is predefined
- No categorical variables

Hierarchical Clustering
- Computationally expensive
- Dendrogram for visualization
- Deterministic
- Number of clusters can be varied
- Can handle missing and categorical values

## Scaling

![[./Images/Pasted image 20240128121232.png]]


![[./Images/Pasted image 20240128121236.png]]

For easier comparison of results rescale your data. Tip: rescale all variables to have variance 1.

![[./Images/Pasted image 20240128121326.png]]
